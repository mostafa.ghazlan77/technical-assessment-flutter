import 'package:get/get.dart';

import '../moduels/auth/login/view.dart';
import '../moduels/blog/add_blog_screen.dart';
import '../moduels/blog/my_blogs_screen.dart';
import '../moduels/blog/blogs_screen.dart';
import '../moduels/home/home_screen.dart';
import '../moduels/search/search_blog_screen.dart';
import '../moduels/search/search_subscribe_screen.dart';
import '../moduels/subscrip/add_subscriber_screen.dart';
import '../moduels/subscrip/subscribe_screen.dart';



List<GetPage<dynamic>> pages = [
  GetPage(name: "/login", page: () => LoginScreen()),
  GetPage(name: "/blog", page: () => BlogScreen()),
  GetPage(name: "/home", page: () => const HomeScreen()),
  GetPage(name: "/myblog", page: () =>  MyBlogScreen()),
  GetPage(name: "/addBlog", page: () =>  AddBlogScreen()),
  GetPage(name: "/search", page: () =>  SearchBlogScreen()),
  GetPage(name: "/subscribers", page: () =>  SubscribersScreen()),
  GetPage(name: "/searchSubscriber", page: () =>  SearchSubscriberScreen()),
  GetPage(name: "/addSubscriber", page: () =>  AddSubscriberScreen()),
];

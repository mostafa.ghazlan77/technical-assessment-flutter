const String baseUrl = "http://10.0.2.2:8000/api";
const String imageRoute = "http://10.0.2.2:8000/storage";


const String loginUri = "$baseUrl/auth/login";
const String logoutUri = "$baseUrl/auth/logout";


//blogs

const String getBlogUri = "$baseUrl/getBlog";
const String deleteBlogUri = "$baseUrl/deleteBlog";
const String updateBlogUri = "$baseUrl/updateBlog";
const String searchBlogsUri = "$baseUrl/searchBlogs";
const String searchTitleBlogsUri = "$baseUrl/searchTitleBlogs";
const String createBlogUri = "$baseUrl/createBlog";

//subscribers

const String getSubscribersUri = "$baseUrl/getSubscribers";
const String searchNameSubscribersUri = "$baseUrl/searchNameSubscribers";
const String searchSubscribersUri = "$baseUrl/searchSubscribers";
const String deleteSubscriberUri = "$baseUrl/deleteSubscriber";
const String createUserUri = "$baseUrl/auth/createUser";
const String updateSubscriberUri = "$baseUrl/updateSubscriber";


import 'package:flutter/widgets.dart';
import 'package:flutter_front/axios.dart';
import 'package:flutter_front/constant/api.dart';
import 'package:get/get.dart';

class SearchBlogController extends GetxController {
  var selectedItem = "Title".obs;
  var searchList = <dynamic>[].obs;
  var isSearch = false.obs;

  TextEditingController searchController = TextEditingController();
  void searchBlog(String type) async {
    searchList.clear();
    isSearch.value = true;
    var response = await Axios.post(
        type == "Title" ? searchTitleBlogsUri : searchBlogsUri,
        {"Text": searchController.text.toString()});
    if (response["status"] == "success") {
      var data = response["data"];
      if (data != null) {
        for (var item in data) {
          searchList.add(item);
        }
      } else {
        searchList.clear();
      }
    }
    isSearch.value = false;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/search/search_blog_controller.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constant/color.dart';
import '../../widget/blog_card.dart';
import '../blog/model.dart';
import '../blog_details/blog_details_screen.dart';

class SearchBlogScreen extends StatelessWidget {
  SearchBlogScreen({super.key});
  final SearchBlogController controller = Get.put(SearchBlogController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[400],
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.grey[800],
          elevation: 0,
          title: Text(
            "Blog Search",
            style:
                TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
          ),
          centerTitle: true,
        ),
        body: ListView(
          children: [
            const SizedBox(
              height: 10,
            ),
            Align(
              alignment: Alignment.center,
              child: Obx(() {
                return DropdownButton(
                  style: GoogleFonts.dmSerifDisplay(
                      fontSize: MediaQuery.of(context).size.width * 0.04,
                      color: Colors.white),
                  dropdownColor: primaryColor,
                  items: <String>['Title', 'All fields'].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (val) {
                    controller.selectedItem.value = val!;
                  },
                  value: controller.selectedItem.value,
                );
              }),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          onChanged: (value) {},
                          controller: controller.searchController,
                          style: const TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: secondaryColor,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  borderSide: BorderSide.none),
                              hintText: "Search here ...".tr,
                              hintStyle: const TextStyle(color: Colors.white),
                              prefixIconColor: Colors.white),
                        ),
                      ),
                      IconButton(
                          onPressed: () {
                            controller
                                .searchBlog(controller.selectedItem.value);
                          },
                          icon: const Icon(Icons.search))
                    ],
                  ),
                ],
              ),
            ),
            Obx(() {
              if (controller.searchList.isEmpty &&
                  controller.isSearch.value == false) {
                return const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                      child: Text(
                    "Enter any keyword and tap the search icon to search ",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  )),
                );
              } else if (controller.isSearch.value) {
                return const Center(
                  child: GFLoader(
                    type: GFLoaderType.ios,
                  ),
                );
              }
              return Column(
                children: [
                  GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 12.0,
                        mainAxisSpacing: 12.0,
                        mainAxisExtent: 450,
                      ),
                      itemCount: controller.searchList.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return BlogCard(
                          blogModel: BlogModel.fromJson(
                              controller.searchList.toList()[index]),
                          onTap: () {
                            Get.to(
                              () => BlogDetailsPage(
                                blogModel: BlogModel.fromJson(
                                  controller.searchList.toList()[index],
                                ),
                              ),
                        
                            );
                          },
                        );
                      }),
                ],
              );
            })
          ],
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/blog/controller.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../widget/button.dart';

import '../../../widget/textfield.dart';
import '../../constant/color.dart';

class EditBlogScreen extends StatefulWidget {
  final Map<String, dynamic> blog;
  const EditBlogScreen({super.key, required this.blog});

  @override
  State<EditBlogScreen> createState() => _EditBlogScreenState();
}

class _EditBlogScreenState extends State<EditBlogScreen> {
  final BlogController controller = Get.find<BlogController>();

  @override
  void initState() {
    controller.titleController.text = widget.blog['Title'];
    controller.contentController.text = widget.blog['Content'];
    controller.statusController.text = widget.blog['Status'];
    controller.dateController.value.text = widget.blog['Publish_Date'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> selectDate() async {
      DateTime? picked = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime(2100));

      if (picked != null) {
        controller.dateController.value.text = picked.toString().split(" ")[0];
      }
    }

    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Edit Blog",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MyTextField(
              hintText: 'Title',
              numberorText: false,
              textController: controller.titleController,
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: 'Content',
              numberorText: false,
              textController: controller.contentController,
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: 'Status',
              numberorText: false,
              textController: controller.statusController,
            ),
            const SizedBox(
              height: 20,
            ),
            Obx(() {
              return Padding(
                padding:
                    const EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
                child: TextField(
                  controller: controller.dateController.value,
                  decoration: const InputDecoration(
                    labelText: "DATE",
                    filled: true,
                    prefixIcon: Icon(Icons.calendar_today),
                    enabledBorder:
                        OutlineInputBorder(borderSide: BorderSide.none),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue)),
                  ),
                  readOnly: true,
                  onTap: () {
                    selectDate();
                  },
                ),
              );
            }),
            Obx(() {
              if (controller.isImageLoading.value == true) {
                return const Padding(
                  padding: EdgeInsets.all(15.0),
                  child: GFLoader(
                    type: GFLoaderType.ios,
                  ),
                );
              }
              if (controller.image == null) {
                return Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: MyButton(
                    text: 'Provider Image',
                    icon: Icons.add_a_photo,
                    onTap: () {
                      controller.uploadImage();
                    },
                  ),
                );
              }
              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: MyButton(
                          text: 'Uploaded',
                          icon: Icons.check,
                          onTap: () {},
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          controller.isImageLoading.value = true;
                          controller.image = null;
                          controller.isImageLoading.value = false;
                        },
                        icon: Icon(
                          Icons.cancel,
                          color: primaryColor,
                        ),
                      ),
                    ],
                  ),
                  Image.file(controller.image!)
                ],
              );
            }),
            Obx(() {
              if (controller.isUpdateBlogs.value) {
                return const GFLoader(
                  type: GFLoaderType.ios,
                );
              }
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: MyButton(
                  text: "Submet",
                  icon: Icons.check,
                  onTap: () {
                    controller.updateBlog(widget.blog['id'].toString());
                  },
                ),
              );
            }),
          ],
        ),
      ),
    );
  }
}

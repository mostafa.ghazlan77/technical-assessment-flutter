import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/blog/model.dart';
import 'package:get/get.dart';
import '../../widget/blog_card.dart';
import '../blog_details/blog_details_screen.dart';
import 'controller.dart';

class BlogScreen extends StatelessWidget {
  BlogScreen({super.key});

  final BlogController controller = Get.put(BlogController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Blogs",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Obx(() {
            if (controller.blogsList.isEmpty &&
                controller.isGetBlogs.value == false) {
              return Column(
                children: [
                  const Text("No data Avaliable"),
                  const SizedBox(
                    height: 10,
                  ),
                  Image.asset("assets/images/no_data.png")
                ],
              );
            }
            return Column(
              children: [
                GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 12.0,
                      mainAxisSpacing: 12.0,
                      mainAxisExtent: 320,
                    ),
                    itemCount: controller.blogsList.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return BlogCard(
                        blogModel: BlogModel.fromJson(
                            controller.blogsList.toList()[index]),
                        onTap: () {
                          Get.to(
                            () => BlogDetailsPage(
                              blogModel: BlogModel.fromJson(
                                controller.blogsList.toList()[index],
                              ),
                            ),
                        
                          );
                        },
                      );
                    }),
              ],
            );
          })
        ],
      ),
    );
  }
}

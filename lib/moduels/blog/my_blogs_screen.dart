import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/blog/model.dart';
import 'package:get/get.dart';
import '../../constant/color.dart';
import '../../widget/blog_card.dart';
import '../blog_details/blog_details_screen.dart';
import 'controller.dart';
import 'edit_blog_screen.dart';

class MyBlogScreen extends StatelessWidget {
  MyBlogScreen({super.key});

  final BlogController controller = Get.put(BlogController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "My Blogs",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Obx(() {
            if (controller.myblogsList.isEmpty &&
                controller.isGetBlogs.value == false) {
              return Column(
                children: [
                  const Text("No data Avaliable"),
                  const SizedBox(
                    height: 10,
                  ),
                  Image.asset("assets/images/no_data.png")
                ],
              );
            }
            return Column(
              children: [
                GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 12.0,
                      mainAxisSpacing: 12.0,
                      mainAxisExtent: 320,
                    ),
                    itemCount: controller.myblogsList.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return BlogCard(
                        blogModel: BlogModel.fromJson(
                            controller.myblogsList.toList()[index]),
                        onTap: () {
                          Get.to(
                            () => BlogDetailsPage(
                              blogModel: BlogModel.fromJson(
                                controller.myblogsList.toList()[index],
                              ),
                            ),
                       
                          );
                        },
                        onEdit: () {
                          Get.to(EditBlogScreen(
                            blog: controller.myblogsList.toList()[index],
                          ));
                        },
                      );
                    }),
              ],
            );
          })
        ],
      ),
      floatingActionButton: IconButton(
        icon: const Icon(
          Icons.edit,
          size: 35,
        ),
        onPressed: () {
          Get.toNamed("addBlog");
        },
        color: Colors.white,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color?>(primaryColor),
        ),
      ),
    );
  }
}

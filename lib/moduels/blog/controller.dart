import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_front/axios.dart';
import 'package:flutter_front/main.dart';
import 'package:flutter_front/moduels/search/search_blog_controller.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../constant/api.dart';

class BlogController extends GetxController {
  var blogsList = <dynamic>[].obs;
  var myblogsList = <dynamic>{}.obs;
  var isGetBlogs = false.obs;
  var isImageLoading = false.obs;
  var isCreatingBlogs = false.obs;
  var isUpdateBlogs = false.obs;
    var isDelete = false.obs;

  TextEditingController titleController = TextEditingController();
  TextEditingController statusController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  Rx<TextEditingController> dateController = TextEditingController().obs;
  final SearchBlogController searchController = Get.put(SearchBlogController());

  File? image;


  deleteBlog(int id) async {
    try {
      isDelete.value = true;
      var response = await Axios.post(deleteBlogUri, {"Id": id.toString()});
      if (response["status"] == "success") {
        removeBlogFromList(id);

        Get.snackbar("Success", "Blog Deleted Successfly",
            backgroundColor: Colors.green);
      }
      isDelete.value = false;
    } catch (e) {
      Get.snackbar("Faild", "Faild to Deleted", backgroundColor: Colors.red);
      isDelete.value = false;
    }
  }

  void getBlogs() async {
    isGetBlogs.value = true;
    var response = await Axios.get(getBlogUri);
    if (response["status"] == "success") {
      var data = response["data"];
      if (data != null) {
        for (var blog in data) {
          blogsList.add(blog);
        }
      } else {
        blogsList.clear();
      }

      if (data != null) {
        for (var blog in data) {
          if (blog["Subscribers_ID"] == sharedPref.getInt("id").toString()) {
            myblogsList.add(blog);
          }
        }
      } else {
        myblogsList.clear();
      }
    }
    isGetBlogs.value = false;
  }

  void removeBlogFromList(int id) {
    blogsList.removeWhere((blog) => blog['id'] == id);
    myblogsList.removeWhere((blog) => blog['id'] == id);
    searchController.searchList.removeWhere((blog) => blog['id'] == id);
  }

  void createBlog() async {
    try {
      isCreatingBlogs.value = true;
      var response = await Axios.postRequestWithFiles(createBlogUri, {
        "Title": titleController.text.toString(),
        "Publish_Date": dateController.value.text,
        "Status": statusController.text.toString(),
        "Subscribers_ID": sharedPref.getInt("id").toString(),
        "Content": contentController.text.toString(),
      }, [
        image!
      ]);
      if (response["status"] == "success") {
        Get.snackbar("Success", "BLog Added Successfly",
            backgroundColor: Colors.green);
      }
      isCreatingBlogs.value = false;
    } catch (e) {
      Get.snackbar("Faild", "Faild to Add Blog", backgroundColor: Colors.red);
      isCreatingBlogs.value = false;
    }
  }

  void uploadImage() async {
    isImageLoading.value = true;
    XFile? xfile = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (xfile != null) {
      image = File(xfile.path);
      isImageLoading.value = false;
    } else {
      Get.snackbar("Image", "Image Not Uploaded");
      isImageLoading.value = false;
    }
  }

  void updateBlog(String id) async {
    try {
      isUpdateBlogs.value = true;
      Map<String, String> data = {
        "Title": titleController.text.toString(),
        "Publish_Date": dateController.value.text,
        "Status": statusController.text.toString(),
        "Content": contentController.text.toString(),
        "Id": id,
        "Image": "",
      };
      List<File> files = [];
      if (image != null) {
        data.remove("Image");
        files.add(image!);
      }
      var response =
          await Axios.postRequestWithFiles(updateBlogUri, data, files);

      if (response["status"] == "success") {
        Get.snackbar("Success", "BLog Updated Successfly",
            backgroundColor: Colors.green);
      }
      isUpdateBlogs.value = false;
    } catch (e) {
      Get.snackbar("Faild", "Faild to update Blog",
          backgroundColor: Colors.red);
      isUpdateBlogs.value = false;
    }
  }

  @override
  void onInit() {
    getBlogs();
    super.onInit();
  }
}

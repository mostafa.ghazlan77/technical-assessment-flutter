class BlogModel {
  int? id;
  String? image;
  String? title;
  String? publishDate;
  String? status;
  String? subscribersID;
  String? content;
  String? createdAt;
  String? updatedAt;

  BlogModel({
    this.content,
    this.image,
    this.publishDate,
    this.status,
    this.subscribersID,
    this.title,
    this.createdAt,
    this.id,
    this.updatedAt,
  });

  BlogModel.fromJson(Map<String, dynamic> json) {
    content = json['Content'];
    image = json['Image'];
    publishDate = json['Publish_Date'];
    status = json['Status'];
    subscribersID = json['Subscribers_ID'];
    title = json['Title'];
    createdAt = json['created_at'];
    id = json['id'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['updated_at'] = updatedAt;
    data['id'] = id;
    data['created_at'] = createdAt;
    data['Title'] = title;
    data['Subscribers_ID'] = subscribersID;
    data['Status'] = status;
    data['Publish_Date'] = publishDate;
    data['Image'] = image;
    data['Content'] = content;

    return data;
  }
}

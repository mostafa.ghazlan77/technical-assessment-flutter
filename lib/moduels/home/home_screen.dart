import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/auth/login/controller.dart';
import 'package:get/get.dart';
import '../../constant/color.dart';
import '../../main.dart';
import '../../widget/card.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

final LoginController controller = Get.put(LoginController());

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    final currentHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Home",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () async {
                await controller.logOut(sharedPref.getString("Username")!);
                Get.offAllNamed("login");
                await sharedPref.clear();
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 25,
            ),
            SizedBox(
              height: currentHeight / 4,
              child: LayoutBuilder(builder: (context, constrains) {
                double width = constrains.maxWidth;
                double height = constrains.maxHeight;
                return Row(
                  children: [
                    Expanded(
                      child: MyCard(
                        height: height,
                        width: width / 2,
                        text: "Blogs",
                        onTap: () {
                          Get.toNamed("blog");
                        },
                        image: "assets/images/blogs.gif",
                      ),
                    ),
                    Expanded(
                      child: MyCard(
                        height: height,
                        width: width / 2,
                        text: "My Blogs",
                        onTap: () {
                          Get.toNamed("myblog");
                        },
                        image: "assets/images/myblog.gif",
                      ),
                    ),
                  ],
                );
              }),
            ),
            SizedBox(
              height: currentHeight / 4,
              child: LayoutBuilder(builder: (context, constrains) {
                double width = constrains.maxWidth;
                double height = constrains.maxHeight;
                return MyCard(
                  height: height,
                  width: width / 2,
                  text: "Subscribers",
                  onTap: () {
                    Get.toNamed("subscribers");
                  },
                  image: "assets/images/Subscribers.gif",
                );
              }),
            ),
          ],
        ),
      ),
      floatingActionButton: IconButton(
        icon: const Icon(
          Icons.search,
          size: 35,
        ),
        onPressed: () {
          Get.toNamed("search");
        },
        color: Colors.white,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color?>(primaryColor),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../widget/button.dart';
import '../../../widget/textfield.dart';
import 'controller.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});
  final LoginController controller = Get.put(LoginController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Login",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MyTextField(
              hintText: "Email",
              numberorText: false,
              textController: controller.userNameController,
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: "Password",
              numberorText: false,
              obscureText: true,
              textController: controller.passwordController,
            ),
            const SizedBox(
              height: 20,
            ),
            Obx(() {
              if (controller.isLogin.value) {
                return const CircularProgressIndicator();
              }
              return MyButton(
                text: "Log in",
                icon: Icons.login,
                onTap: () async {
                  await controller.logIn();
                },
              );
            })
          ],
        ),
      ),
    );
  }
}

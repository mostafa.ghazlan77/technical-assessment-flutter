import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../axios.dart';
import '../../../constant/api.dart';
import '../../../main.dart';

class LoginController extends GetxController {
  var isLogin = false.obs;
  var isLogOut = false.obs;
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  logIn() async {
    isLogin.value = true;
    var response = await Axios.post(loginUri, {
      "Username": userNameController.text,
      "Password": passwordController.text,
    });
    if (response["status"] == "success") {
      var data = response["data"];
      await sharedPref.setString("Username", userNameController.text);
      await sharedPref.setInt("id", data["id"]);
      Get.offAllNamed("home");
      Get.snackbar("Success", response["message"]);
    } else {
      Get.snackbar("Faild", response["message"]);
    }
    isLogin.value = false;
  }

  logOut(String userName) async {
    isLogOut.value = true;
    var response = await Axios.post(logoutUri, {
      "Username": userName,
    });
    if (response["status"] == "success") {
      Get.offAllNamed("login");
      sharedPref.clear();
      Get.snackbar("Success", response["message"]);
    } else {
      Get.snackbar("Faild", response["message"]);
    }
    isLogOut.value = false;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/search/search_subscriber_controller.dart';
import 'package:get/get.dart';

import '../../axios.dart';
import '../../constant/api.dart';

class SubscribersController extends GetxController {
  var isGetSubscriber = false.obs;
  var isDelete = false.obs;
  var isCreating = false.obs;
  var isUpdate = false.obs;
  TextEditingController nameController = TextEditingController();
  TextEditingController statusController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  var subscriberList = <dynamic>[].obs;
  final SearchSubscriberController searchController =
      Get.put(SearchSubscriberController());

  void getSubscribers() async {
    isGetSubscriber.value = true;
    var response = await Axios.get(getSubscribersUri);
    if (response["status"] == "success") {
      var data = response["data"];
      if (data != null) {
        for (var subscriber in data) {
          subscriberList.add(subscriber);
        }
      } else {
        subscriberList.clear();
      }
    }
    isGetSubscriber.value = false;
  }

  void removeSubscribFromList(int id) {
    subscriberList.removeWhere((subscriber) => subscriber['id'] == id);
    searchController.searchList
        .removeWhere((subscriber) => subscriber['id'] == id);
  }

  void deleteSubscriber(int id) async {
    try {
      isDelete.value = true;
      var response =
          await Axios.post(deleteSubscriberUri, {"Id": id.toString()});
      if (response["status"] == "success") {
        removeSubscribFromList(id);
        Get.snackbar("Success", "Blog Deleted Successfly",
            backgroundColor: Colors.green);
      }
      isDelete.value = false;
    } catch (e) {
      Get.snackbar("Faild", "Faild to Deleted", backgroundColor: Colors.red);
      isDelete.value = false;
    }
  }

  void createSubscriber() async {
    try {
      isCreating.value = true;
      var response = await Axios.post(
        createUserUri,
        {
          "Name": nameController.text.toString(),
          "Username": userNameController.text.toString(),
          "Status": statusController.text.toString(),
          "Password": passwordController.text.toString(),
        },
      );
      if (response["status"] == "success") {
        Get.snackbar("Success", response["message"],
            backgroundColor: Colors.green);
      }
      isCreating.value = false;
    } catch (e) {
      Get.snackbar("Faild", e.toString(), backgroundColor: Colors.red);
      isCreating.value = false;
    }
  }

  void updateSubscribe(String id) async {
    try {
      isUpdate.value = true;
      Map<String, String> data = {
        "Name": nameController.text.toString(),
        "Username": userNameController.text.toString(),
        "Status": statusController.text.toString(),
        "Password": passwordController.text.toString(),
        "Id": id,
      };

      var response = await Axios.post(
        updateSubscriberUri,
        data,
      );

      if (response["status"] == "success") {
        Get.snackbar("Success", "Updated Successfly",
            backgroundColor: Colors.green);
      }
      isUpdate.value = false;
    } catch (e) {
      Get.snackbar("Faild", "Faild to update Blog",
          backgroundColor: Colors.red);
      isUpdate.value = false;
    }
  }

  @override
  void onInit() {
    getSubscribers();
    super.onInit();
  }
}

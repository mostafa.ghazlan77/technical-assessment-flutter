import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/subscrip/controller.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../../../widget/button.dart';

import '../../../widget/textfield.dart';

class UpdateSubscriberScreen extends StatefulWidget {
  final Map<String, dynamic> subscribe;
  const UpdateSubscriberScreen({super.key, required this.subscribe});

  @override
  State<UpdateSubscriberScreen> createState() => _UpdateSubscriberScreenState();
}

class _UpdateSubscriberScreenState extends State<UpdateSubscriberScreen> {
  final SubscribersController controller = Get.find<SubscribersController>();

  @override
  void initState() {
    controller.nameController.text = widget.subscribe['Name'];
    controller.userNameController.text = widget.subscribe['Username'];
    controller.statusController.text = widget.subscribe['Status'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Update Subscriber",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MyTextField(
              hintText: 'Name',
              numberorText: false,
              textController: controller.nameController,
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: 'User Name',
              numberorText: false,
              textController: controller.userNameController,
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: 'Status',
              numberorText: false,
              textController: controller.statusController,
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: 'Password',
              numberorText: false,
              textController: controller.passwordController,
            ),
            const SizedBox(
              height: 20,
            ),
            Obx(() {
              if (controller.isUpdate.value) {
                return const GFLoader(
                  type: GFLoaderType.ios,
                );
              }
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: MyButton(
                  text: "Submet",
                  icon: Icons.check,
                  onTap: () {
                    controller.updateSubscribe(widget.subscribe['id'].toString());
                  },
                ),
              );
            }),
          ],
        ),
      ),
    );
  }
}

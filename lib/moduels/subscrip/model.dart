class SubscribersModel {
  int? id;
  String? name;
  String? userName;
  String? password;
  String? status;
  String? deviceId;
  String? createdAt;
  String? updatedAt;

  SubscribersModel({
    this.deviceId,
    this.name,
    this.password,
    this.status,
    this.userName,
    this.createdAt,
    this.id,
    this.updatedAt,
  });

  SubscribersModel.fromJson(Map<String, dynamic> json) {
    deviceId = json['device_id'];
    name = json['Name'];
    password = json['Password'];
    status = json['Status'];
    userName = json['Username'];
    createdAt = json['created_at'];
    id = json['id'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['updated_at'] = updatedAt;
    data['id'] = id;
    data['created_at'] = createdAt;
    data['device_id'] = deviceId;
    data['Username'] = userName;
    data['Status'] = status;
    data['Password'] = password;
    data['Name'] = name;

    return data;
  }
}

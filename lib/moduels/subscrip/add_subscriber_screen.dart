import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/subscrip/controller.dart';
import 'package:flutter_front/widget/textfield.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
import '../../widget/button.dart';

class AddSubscriberScreen extends StatelessWidget {
  AddSubscriberScreen({super.key});
  final SubscribersController controller = Get.find<SubscribersController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Add new Subscriber",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            MyTextField(
              hintText: "Name",
              numberorText: false,
              textController: controller.nameController,
            ),
            const SizedBox(
              height: 10,
            ),
            MyTextField(
              hintText: "Username",
              numberorText: false,
              textController: controller.userNameController,
            ),
            const SizedBox(
              height: 10,
            ),
            MyTextField(
              hintText: "Status",
              numberorText: false,
              textController: controller.statusController,
            ),
            const SizedBox(
              height: 10,
            ),
            MyTextField(
              hintText: "Password",
              numberorText: false,
              textController: controller.passwordController,
            ),
            const SizedBox(
              height: 10,
            ),
            Obx(() {
              if (controller.isCreating.value) {
                return const GFLoader(
                  type: GFLoaderType.ios,
                );
              }
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: MyButton(
                  text: "Submet",
                  icon: Icons.check,
                  onTap: () {
                    controller.createSubscriber();
                  },
                ),
              );
            }),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/subscrip/controller.dart';
import 'package:flutter_front/moduels/subscrip/model.dart';
import 'package:flutter_front/widget/subscribe_card.dart';
import 'package:get/get.dart';

import '../../constant/color.dart';
import 'update_subscriber_screen.dart';

class SubscribersScreen extends StatelessWidget {
  SubscribersScreen({super.key});
  final SubscribersController controller = Get.put(SubscribersController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Subscribers",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Obx(() {
            if (controller.subscriberList.isEmpty &&
                controller.isGetSubscriber.value == false) {
              return Column(
                children: [
                  const Text("No data Avaliable"),
                  const SizedBox(
                    height: 10,
                  ),
                  Image.asset("assets/images/no_data.png")
                ],
              );
            }
            return Column(
              children: [
                GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 12.0,
                      mainAxisSpacing: 12.0,
                      mainAxisExtent: 360,
                    ),
                    itemCount: controller.subscriberList.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return SubscribeCard(
                        subscribeModel: SubscribersModel.fromJson(
                            controller.subscriberList.toList()[index]),
                  
                        onEdit: () {
                          Get.to(() => UpdateSubscriberScreen(
                                subscribe:
                                    controller.subscriberList.toList()[index],
                              ));
                        },
                      );
                    }),
              ],
            );
          })
        ],
      ),
      floatingActionButton: Stack(
        children: [
          Positioned(
            bottom: 80,
            right: 10,
            child: IconButton(
              icon: const Icon(
                Icons.add,
                size: 35,
              ),
              onPressed: () {
                Get.toNamed("addSubscriber");
              },
              color: Colors.white,
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color?>(primaryColor),
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            right: 10,
            child: IconButton(
              icon: const Icon(
                Icons.search,
                size: 35,
              ),
              onPressed: () {
                Get.toNamed("searchSubscriber");
              },
              color: Colors.white,
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color?>(primaryColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

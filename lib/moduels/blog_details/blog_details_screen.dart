import 'package:flutter_front/moduels/blog/controller.dart';
import 'package:flutter_front/moduels/blog/model.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constant/api.dart';
import '../../constant/color.dart';

class BlogDetailsPage extends StatefulWidget {
  final BlogModel blogModel;

  const BlogDetailsPage({
    super.key,
    required this.blogModel,
  });

  @override
  State<BlogDetailsPage> createState() => _BlogDetailsPageState();
}

class _BlogDetailsPageState extends State<BlogDetailsPage> {
  BlogController controller = Get.find<BlogController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey[800],
        elevation: 0,
        title: Text(
          "Blog Details",
          style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.05),
        ),
        centerTitle: true,
      ),
      body: Column(children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: ListView(
              children: [
                widget.blogModel.image.toString() == ""
                    ? Image.asset(
                        "assets/images/404.png",
                        height: 200,
                      )
                    : Image.network(
                        "$imageRoute/${widget.blogModel.image}",
                        fit: BoxFit.cover,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) {
                            return child;
                          } else {
                            return Center(
                              child: CircularProgressIndicator(
                                color: secondaryColor,
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        (loadingProgress.expectedTotalBytes ??
                                            1)
                                    : null,
                              ),
                            );
                          }
                        },
                        errorBuilder: (BuildContext context, Object error,
                            StackTrace? stackTrace) {
                          return Image.asset("assets/images/404.png");
                        },
                      ),
                const SizedBox(
                  height: 25,
                ),
                Text(
                  widget.blogModel.title!,
                  style: GoogleFonts.dmSerifDisplay(
                    fontSize: MediaQuery.of(context).size.width * 0.06,
                  ),
                ),
                const SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${"ID".tr}:",
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.bold,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                    Text(
                      widget.blogModel.id.toString(),
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                  ],
                ),
                const Divider(color: Colors.white),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${"Publish_Date".tr}:",
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.bold,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                    Text(
                      widget.blogModel.publishDate!,
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                  ],
                ),
                const Divider(color: Colors.white),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${"Status".tr}:",
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.bold,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                    Text(
                      widget.blogModel.status.toString(),
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                  ],
                ),
                const Divider(color: Colors.white),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${"Content".tr}:",
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.bold,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                    Text(
                      widget.blogModel.content.toString(),
                      style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.04),
                    ),
                  ],
                ),
                const Divider(color: Colors.white),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "${"Subscribers_ID".tr}:",
                        style: TextStyle(
                            color: Colors.grey[900],
                            fontWeight: FontWeight.bold,
                            fontSize: MediaQuery.of(context).size.width * 0.04),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.blogModel.subscribersID.toString(),
                        style: TextStyle(
                          color: Colors.grey[900],
                          fontWeight: FontWeight.w600,
                          fontSize: MediaQuery.of(context).size.width * 0.04,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.circular(16),
                color: Colors.grey[200],
              ),
              child: IconButton(
                icon: const Icon(
                  Icons.delete,
                  size: 30,
                  color: Colors.red,
                ),
                onPressed: () async {
                  controller.deleteBlog(
                    widget.blogModel.id!,
                  );
                },
              ),
            ),
          ),
        ),
      ]),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_front/moduels/subscrip/controller.dart';

import 'package:flutter_front/moduels/subscrip/model.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../constant/color.dart';

class SubscribeCard extends StatelessWidget {
  final SubscribersModel subscribeModel;
  final void Function()? onTap;
  final void Function()? onEdit;
  SubscribeCard({
    super.key,
    this.onTap,
    required this.subscribeModel,
    this.onEdit,
  });
  final SubscribersController controller = Get.put(SubscribersController());

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        color: primaryColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
                width: double.infinity,
                height: 150,
                child: Image.asset(
                  "assets/images/profile_image.jpg",
                  fit: BoxFit.fitWidth,
                )),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 3, top: 3),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                  text: "Name : ${subscribeModel.name}",
                  style: GoogleFonts.dmSerifDisplay(
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const Divider(color: Colors.grey),
            Padding(
              padding: const EdgeInsets.only(left: 3, top: 3),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                  text: "User Name : ${subscribeModel.userName}",
                  style: GoogleFonts.dmSerifDisplay(
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const Divider(color: Colors.grey),
            Padding(
              padding: const EdgeInsets.only(left: 3, top: 3),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                  text: "Status : ${subscribeModel.status}",
                  style: GoogleFonts.dmSerifDisplay(
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(16),
                      color: Colors.grey[200],
                    ),
                    child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          size: 30,
                          color: Colors.yellow.shade700,
                        ),
                        onPressed: onEdit),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(16),
                      color: Colors.grey[200],
                    ),
                    child: IconButton(
                      icon: const Icon(
                        Icons.delete,
                        size: 30,
                        color: Colors.red,
                      ),
                      onPressed: () async {
                        controller.deleteSubscriber(
                          subscribeModel.id!,
                        );
                      },
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

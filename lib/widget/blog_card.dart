import 'package:flutter/material.dart';
import 'package:flutter_front/main.dart';
import 'package:flutter_front/moduels/blog/controller.dart';
import 'package:flutter_front/moduels/blog/model.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../constant/api.dart';
import '../constant/color.dart';

class BlogCard extends StatelessWidget {
  final BlogModel blogModel;
  final void Function()? onTap;
  final void Function()? onEdit;

  final BlogController controller = Get.put(BlogController());

  BlogCard({
    super.key,
    this.onTap,
    required this.blogModel,
    this.onEdit,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        color: primaryColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              height: 150,
              child: Image.network(
                "$imageRoute/${blogModel.image}",
                fit: BoxFit.fitWidth,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent? loadingProgress) {
                  if (loadingProgress == null) {
                    return child;
                  } else {
                    return Center(
                      child: CircularProgressIndicator(
                        color: secondaryColor,
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                (loadingProgress.expectedTotalBytes ?? 1)
                            : null,
                      ),
                    );
                  }
                },
                errorBuilder: (BuildContext context, Object error,
                    StackTrace? stackTrace) {
                  return Image.asset("assets/images/404.png");
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 3, top: 3),
              child: Text(
                "Title : ${blogModel.title}",
                style: GoogleFonts.dmSerifDisplay(
                  fontSize: MediaQuery.of(context).size.width * 0.04,
                  color: Colors.white,
                ),
              ),
            ),
            const Divider(color: Colors.grey),
            Padding(
              padding: const EdgeInsets.only(left: 3, top: 3),
              child: RichText(
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                text: TextSpan(
                  text: "Content : ${blogModel.content}",
                  style: GoogleFonts.dmSerifDisplay(
                    fontSize: MediaQuery.of(context).size.width * 0.04,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            blogModel.subscribersID == sharedPref.getInt("id").toString()
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.grey[200],
                          ),
                          child: IconButton(
                              icon: Icon(
                                Icons.edit,
                                size: 30,
                                color: Colors.yellow.shade700,
                              ),
                              onPressed: onEdit),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: const EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.grey[200],
                          ),
                          child: IconButton(
                            icon: const Icon(
                              Icons.delete,
                              size: 30,
                              color: Colors.red,
                            ),
                            onPressed: () async {
                              controller.deleteBlog(
                                blogModel.id!,
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  )
                : Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.grey[200],
                        ),
                        child: IconButton(
                          icon: const Icon(
                            Icons.delete,
                            size: 30,
                            color: Colors.red,
                          ),
                          onPressed: () async {
                            controller.deleteBlog(
                              blogModel.id!,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}

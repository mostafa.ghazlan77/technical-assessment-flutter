import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

import '../constant/color.dart';


class MyCard extends StatelessWidget {
  final String text;
  final String? image;
  final double? width;
  final double? height;
  final String? animation;
  final void Function()? onTap;
  const MyCard({
    super.key,
    required this.text,
    required this.onTap,
    this.image,
    this.animation,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizedBox(
        width: width,
        height: height,
        child: Card(
          color: secondaryColor,
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                title: Center(
                  child: Text(
                    text,
                    style: GoogleFonts.dmSerifDisplay(
                        fontSize: MediaQuery.of(context).size.width * 0.04,
                        color: Colors.white),
                  ),
                ),
              ),
              if (image != null) Image.asset(image!, height: height! / 2),
              if (animation != null)
                Lottie.asset(animation!, height: height! / 2),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore_for_file: avoid_print

import "dart:convert";
import "dart:io";
import 'package:path/path.dart';

import "package:http/http.dart" as http;

class Axios {
  static final _headrs = {'Content-Type': 'application/json'};
  static Future get(url) async {
    final res = await http.get(Uri.parse(url), headers: _headrs);
    return jsonDecode(res.body);
  }

  static Future post(url, body) async {
    final res = await http.post(Uri.parse(url),
        headers: _headrs, body: jsonEncode(body));
    return jsonDecode(res.body);
  }

 static Future postRequestWithFiles(String url, Map data, List<File> files) async {
    var request = http.MultipartRequest("POST", Uri.parse(url));
    request.headers.addAll(_headrs);

    for (var i = 0; i < files.length; i++) {
      var length = await files[i].length();
      var stream = http.ByteStream(files[i].openRead());
      var multipartFile = http.MultipartFile(
        i == 0 ? "Image" : "Image2",
        stream,
        length,
        filename: basename(files[i].path),
      );
      request.files.add(multipartFile);
    }

    data.forEach((key, value) {
      request.fields[key] = value;
    });

    var myrequest = await request.send();
    var response = await http.Response.fromStream(myrequest);
    if (myrequest.statusCode == 200) {
      print(response.body);
      return jsonDecode(response.body);
    } else {
      print("Error ${myrequest.statusCode}");
    }
  }
}
